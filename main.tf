# https://www.terraform.io/docs/configuration/resources.html#for_each-multiple-resource-instances-defined-by-a-map-or-set-of-strings
resource "aws_ssm_parameter" "string_parameters" {
  for_each  = toset(var.string_parameters)
  name      = "${local.prefix}${each.value}"
  type      = "String"
  value     = local.default_value
  tier      = "Standard"
  overwrite = var.overwrite
  lifecycle {
    ignore_changes = [
      value,
    ]
  }
}

resource "aws_ssm_parameter" "securestring_parameters" {
  for_each  = toset(var.securestring_parameters)
  name      = "${local.prefix}${each.value}"
  type      = "SecureString"
  value     = local.default_value
  tier      = "Standard"
  key_id    = var.key_id
  overwrite = var.overwrite
  lifecycle {
    ignore_changes = [
      value,
    ]
  }
}


resource "aws_ssm_parameter" "stringlist_parameters" {
  for_each  = toset(var.stringlist_parameters)
  name      = "${local.prefix}${each.value}"
  type      = "StringList"
  value     = local.default_value
  tier      = "Standard"
  overwrite = var.overwrite
  lifecycle {
    ignore_changes = [
      value,
    ]
  }
}
