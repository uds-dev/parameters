variable "key_id" {
  description = "When using SecureString, use a specific KMS key, defaults to - alias/aws/ssm"
  type        = string
  default     = "alias/aws/ssm"
}

variable "overwrite" {
  type    = bool
  default = false
}

variable "prefix" {
  type    = string
  default = ""
}

variable "string_parameters" {
  type    = list(string)
  default = []
}

variable "securestring_parameters" {
  type    = list(string)
  default = []
}

variable "stringlist_parameters" {
  type        = list(string)
  default     = []
  description = "StringList (comma-separated)"
}

locals {
  default_value = "empty"
  prefix        = var.prefix != "" ? var.prefix : ""
}
